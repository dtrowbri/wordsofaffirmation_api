﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WordsOfAffirmation_API.Models;
using System.Data.SqlClient;
using System.Configuration;

namespace WordsOfAffirmation_API.DAO
{
    public class BasicMessageDAO
    {
        private string connectionString = ConfigurationManager.ConnectionStrings["WOA_Connection_String"].ConnectionString;

        private string AddBasicMessageQuery = "Insert into BasicMessages (Message) values (@Message);";

        private string UpdateBasicMessageQuery = "Update BasicMessages set Message = @Message where MessageId = @MessageId;";

        private string DeleteBasicMessageQuery = "Delete BasicMessages where MessageId = @MessageId;";

        private string GetBasicMessageQuery = "select MessageId, Message from BasicMessages;";

        public bool AddBasicMessage(BasicMessageModel message)
        {
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                using(SqlCommand cmd = new SqlCommand(AddBasicMessageQuery, conn))
                {
                    cmd.Parameters.AddWithValue("@Message", message.Message);
                    bool succeeded = false;
                    try
                    {
                        conn.Open();
                        cmd.Transaction = conn.BeginTransaction();
                        int affectedRows = cmd.ExecuteNonQuery();

                        if(affectedRows != 1)
                        {
                            cmd.Transaction.Rollback();
                        } else
                        {
                            cmd.Transaction.Commit();
                            succeeded = true;
                        }
                        

                    }catch(SqlException ex)
                    {
                        Console.Error.WriteLine(ex.Message);
                    }catch(Exception ex)
                    {
                        Console.Error.WriteLine(ex.Message);
                    }
                    conn.Close();
                    return succeeded;
                }
            }
        }
    
        public bool UpdateBasicMessage(BasicMessageModel message)
        {
            using(SqlConnection conn = new SqlConnection(connectionString))
            {
                using(SqlCommand cmd = new SqlCommand(UpdateBasicMessageQuery, conn))
                {
                    cmd.Parameters.AddWithValue("@Message", message.Message);
                    cmd.Parameters.AddWithValue("@MessageId", message.MessageId);
                    bool succeeded = false;
                    try
                    {
                        conn.Open();
                        cmd.Transaction = conn.BeginTransaction();
                        int affectedRows = cmd.ExecuteNonQuery();

                        if(affectedRows != 1)
                        {
                            cmd.Transaction.Rollback();
                        } else
                        {
                            cmd.Transaction.Commit();
                            succeeded = true;
                        }
                        
                    }catch(SqlException ex)
                    {
                        Console.Error.WriteLine(ex.Message);
                    }catch(Exception ex)
                    {
                        Console.Error.WriteLine(ex.Message);
                    }
                    conn.Close();
                    return succeeded;
                }
            }
        }

        public bool DeleteBasicMessage(BasicMessageModel message)
        {
            using(SqlConnection conn = new SqlConnection(connectionString))
            {
                using(SqlCommand cmd = new SqlCommand(DeleteBasicMessageQuery, conn))
                {
                    cmd.Parameters.AddWithValue("@MessageId", message.MessageId);
                    bool succeeded = false;

                    try
                    {
                        conn.Open();
                        cmd.Transaction = conn.BeginTransaction();
                        int affectedRows = cmd.ExecuteNonQuery();

                        if(affectedRows != 1)
                        {
                            cmd.Transaction.Rollback();
                        } else
                        {
                            cmd.Transaction.Commit();
                            succeeded = true;
                        }

                    }catch(SqlException ex)
                    {
                        Console.Error.WriteLine(ex.Message);
                    }catch(Exception ex)
                    {
                        Console.Error.WriteLine(ex.Message);
                    }
                    conn.Close();
                    return succeeded;
                }
            }
        }
    
        public List<BasicMessageModel> GetBasicMessages()
        {
            using(SqlConnection conn = new SqlConnection(connectionString))
            {
                using(SqlCommand cmd = new SqlCommand(GetBasicMessageQuery, conn))
                {
                    conn.Open();
                    List<BasicMessageModel> messages = new List<BasicMessageModel>();
                    try
                    {
                        SqlDataReader reader = cmd.ExecuteReader();
                        
                        while (reader.Read())
                        {
                            BasicMessageModel message = new BasicMessageModel();
                            message.MessageId = Convert.ToInt32(reader["MessageId"]);
                            message.Message = reader["Message"].ToString();
                            messages.Add(message);
                        }
                    }catch(SqlException ex)
                    {
                        Console.Error.WriteLine(ex.Message);
                    }catch(Exception ex)
                    {
                        Console.Error.WriteLine(ex.Message);
                    }
                    conn.Close();
                    return messages;
                }
            }
        }
    }
}