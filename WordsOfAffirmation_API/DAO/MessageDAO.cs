﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WordsOfAffirmation_API.Models;
using System.Data.SqlClient;
using System.Configuration;


namespace WordsOfAffirmation_API.DAO
{
    public class MessageDAO
    {

        private string connectionString = ConfigurationManager.ConnectionStrings["WOA_Connection_String"].ConnectionString;

        private string MessageAddQuery = "Insert into Messages (UserId, RecipientName, RecipientPhoneNumber, Message, Frequency, Archived) values (@UserId, @RecipientName, @RecipientPhoneNumber, @Message, @Frequency, @Archived);";

        private string UserMessageGetQuery = "Select MessageId, UserId, RecipientName, RecipientPhoneNumber, Message, Frequency from Messages where UserId = @UserId and Archived = 0;";

        private string UserMessageUpdateQuery = "Update Messages set RecipientName = @RecipientName, RecipientPhoneNumber = @RecipientPhoneNumber, Message = @Message, Frequency = @Frequency, Archived = @Archived where MessageId = @MessageId;";

        public bool AddMessage(MessageModel message)
        {
            using(SqlConnection conn = new SqlConnection(connectionString))
            {
                using(SqlCommand cmd = new SqlCommand(MessageAddQuery, conn))
                {
                    cmd.Parameters.AddWithValue("@UserId", message.UserId);
                    cmd.Parameters.AddWithValue("@RecipientName", message.RecipientName);
                    cmd.Parameters.AddWithValue("@RecipientPhoneNumber", message.RecipientPhoneNumber);
                    cmd.Parameters.AddWithValue("@Message", message.Message);
                    cmd.Parameters.AddWithValue("@Frequency", message.Frequency);
                    cmd.Parameters.AddWithValue("@Archived", 0);
                    bool succeeeded = false;
                    try
                    {
                        conn.Open();
                        cmd.Transaction = conn.BeginTransaction();
                        int rowsAffected = cmd.ExecuteNonQuery();
                        
                        if(rowsAffected != 1)
                        {
                            cmd.Transaction.Rollback();
                        }else
                        {
                            cmd.Transaction.Commit();
                            succeeeded = true;
                        }

                    }catch(SqlException ex)
                    {
                        Console.Error.WriteLine(ex.Message);
                    }catch(Exception ex)
                    {
                        Console.Error.WriteLine(ex.Message);
                    }
                    conn.Close();
                    return succeeeded;
                }
            }
        }

        public List<MessageModel> getUserMessages(int userid)
        {
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                using (SqlCommand cmd = new SqlCommand(UserMessageGetQuery, conn))
                {
                    cmd.Parameters.AddWithValue("@UserId", userid);

                    try
                    {
                        conn.Open();
                        SqlDataReader reader = cmd.ExecuteReader();

                        List<MessageModel> messages = new List<MessageModel>();
                        while(reader.Read())
                        {
                            MessageModel message = new MessageModel();
                            message.MessageId = Convert.ToInt32(reader["MessageId"]);
                            message.UserId = Convert.ToInt32(reader["UserId"]);
                            message.RecipientName = reader["RecipientName"].ToString();
                            message.RecipientPhoneNumber = reader["RecipientPhoneNumber"].ToString();
                            message.Message = reader["Message"].ToString();
                            message.Frequency = Convert.ToInt32(reader["Frequency"]);
                            messages.Add(message);
                        }

                        conn.Close();
                        return messages;
                    }
                    catch (SqlException ex)
                    {
                        Console.Error.WriteLine(ex.Message);
                    }
                    catch (Exception ex)
                    {
                        Console.Error.WriteLine(ex.Message);
                    }
                    conn.Close();
                    return new List<MessageModel>();
                }
            }
        }

        public bool updateUserMessage(MessageModel message)
        {
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                using (SqlCommand cmd = new SqlCommand(UserMessageUpdateQuery, conn))
                {
                    cmd.Parameters.AddWithValue("@MessageId", message.MessageId);
                    cmd.Parameters.AddWithValue("@UserId", message.UserId);
                    cmd.Parameters.AddWithValue("@RecipientName", message.RecipientName);
                    cmd.Parameters.AddWithValue("@RecipientPhoneNumber", message.RecipientPhoneNumber);
                    cmd.Parameters.AddWithValue("@Message", message.Message);
                    cmd.Parameters.AddWithValue("@Frequency", message.Frequency);
                    cmd.Parameters.AddWithValue("@Archived", message.Archived);
                    bool succeeded = false;
                    try
                    {
                        conn.Open();
                        cmd.Transaction = conn.BeginTransaction();
                        int affectedRows = cmd.ExecuteNonQuery();

                        if(affectedRows != 1)
                        {
                            cmd.Transaction.Rollback();
                        } else
                        {
                            cmd.Transaction.Commit();
                            succeeded = true;
                        }
                        
                    }
                    catch (SqlException ex)
                    {
                        Console.Error.WriteLine(ex.Message);
                    }
                    catch (Exception ex)
                    {
                        Console.Error.WriteLine(ex.Message);
                    }
                    conn.Close();
                    return succeeded;
                }
            }
        }

    }
}