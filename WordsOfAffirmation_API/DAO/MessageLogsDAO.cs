﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WordsOfAffirmation_API.Models;
using System.Data.SqlClient;
using System.Configuration;


namespace WordsOfAffirmation_API.DAO
{
    public class MessageLogsDAO
    {
        private string connectionString = ConfigurationManager.ConnectionStrings["WOA_Connection_String"].ConnectionString;

        private string AddMessageLogQuery = "Insert into MessageLogs (UserId, Recipient, Message, DeliveredOn) values(@UserId, @Recipient, @Message, @DeliveredOn);";

        private string GetMessageLogsQuery = "Select LogId, UserId, Recipient, Message, DeliveredOn from MessageLogs;";

        public bool AddMessageLog(MessageLogsModel log)
        {
            using(SqlConnection conn = new SqlConnection(connectionString))
            {
                using(SqlCommand cmd = new SqlCommand(AddMessageLogQuery, conn))
                {
                    cmd.Parameters.AddWithValue("@UserId", log.UserId);
                    cmd.Parameters.AddWithValue("@Recipient", log.Recipient);
                    cmd.Parameters.AddWithValue("@Message", log.Message);
                    cmd.Parameters.AddWithValue("@DeliveredOn", log.DeliveredOn);
                    bool succeeded = true;
                    try
                    {
                        conn.Open();
                        cmd.Transaction = conn.BeginTransaction();
                        int affectedRows = cmd.ExecuteNonQuery();

                        if(affectedRows != 1)
                        {
                            cmd.Transaction.Rollback();
                        }else
                        {
                            cmd.Transaction.Commit();
                            succeeded = true;
                        }
                    }catch(SqlException ex)
                    {
                        Console.Error.WriteLine(ex.Message);
                    }catch(Exception ex)
                    {
                        Console.Error.WriteLine(ex.Message);
                    }
                    conn.Close();
                    return succeeded;
                }
            }
        }

        public List<MessageLogsModel> GetMessageLogs()
        {
            using(SqlConnection conn = new SqlConnection(connectionString))
            {
                using(SqlCommand cmd = new SqlCommand(GetMessageLogsQuery, conn))
                {
                    List <MessageLogsModel> logs = new List<MessageLogsModel>();
                    try
                    {
                        conn.Open();
                        SqlDataReader reader = cmd.ExecuteReader();

                        while (reader.Read())
                        {
                            MessageLogsModel log = new MessageLogsModel();
                            log.LogId = Convert.ToInt32(reader["LogId"]);
                            log.UserId = Convert.ToInt32(reader["UserId"]);
                            log.Recipient = reader["Recipient"].ToString();
                            log.Message = reader["Message"].ToString();
                            log.DeliveredOn = Convert.ToDateTime(reader["DeliveredOn"]);
                            logs.Add(log);
                        }
                    }catch(SqlException ex)
                    {
                        Console.Error.WriteLine(ex.Message);
                    }catch(Exception ex)
                    {
                        Console.Error.WriteLine(ex.Message);
                    }
                    conn.Close();
                    return logs;
                }
            }
        }
    }
}