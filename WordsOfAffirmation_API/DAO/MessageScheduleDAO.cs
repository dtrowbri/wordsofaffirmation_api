﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WordsOfAffirmation_API.Models;
using System.Data.SqlClient;
using System.Configuration;


namespace WordsOfAffirmation_API.DAO
{
    public class MessageScheduleDAO
    {
        private string connectionString = ConfigurationManager.ConnectionStrings["WOA_Connection_String"].ConnectionString;

        private string AddMessageScheduleQuery = "Insert into MessageSchedule (MessageId, NextSendTime) values (@MessageId, @NextSendTime);";

        private string UpdateMessageScheduleQuery = "Update MessageSchedule set NextSendTime = @NextSendTime where MessageScheduleId = @MessageScheduleId;";

        private string DeleteMessageScheduleQuery = "Delete from MessageSchedule where MessageScheduleId = @MessageScheduleId;";

        

        public bool AddMessageSchedule(MessageScheduleModel message)
        {
            using(SqlConnection conn = new SqlConnection(connectionString))
            {
                using(SqlCommand cmd = new SqlCommand(AddMessageScheduleQuery, conn))
                {
                    cmd.Parameters.AddWithValue("@MessageId", message.MessageId);
                    cmd.Parameters.AddWithValue("@NextSendTime", message.NextSendTime);
                    bool succeeded = false;
                    try
                    {
                        conn.Open();
                        cmd.Transaction = conn.BeginTransaction();
                        int affectedRows = cmd.ExecuteNonQuery();

                        if(affectedRows != 1)
                        {
                            cmd.Transaction.Rollback();
                        }
                        else
                        {
                            cmd.Transaction.Commit();
                            succeeded = true;
                        }

                    }catch(SqlException ex)
                    {
                        Console.Error.WriteLine(ex.Message);
                    }catch(Exception ex)
                    {
                        Console.Error.WriteLine(ex.Message);
                    }
                    conn.Close();
                    return succeeded;
                }
            }
        }

        public bool UpdateMessageSchedule(MessageScheduleModel message)
        {
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                using (SqlCommand cmd = new SqlCommand(UpdateMessageScheduleQuery, conn))
                {
                    cmd.Parameters.AddWithValue("@NextSendTime", message.NextSendTime);
                    cmd.Parameters.AddWithValue("@MessageScheduleId", message.MessageScheduleId);
                    bool succeeded = false;
                    try
                    {
                        conn.Open();
                        cmd.Transaction = conn.BeginTransaction();
                        int affectedRows = cmd.ExecuteNonQuery();

                        if (affectedRows != 1)
                        {
                            cmd.Transaction.Rollback();
                        }
                        else
                        {
                            cmd.Transaction.Commit();
                            succeeded = true;
                        }

                    }
                    catch (SqlException ex)
                    {
                        Console.Error.WriteLine(ex.Message);
                    }
                    catch (Exception ex)
                    {
                        Console.Error.WriteLine(ex.Message);
                    }
                    conn.Close();
                    return succeeded;
                }
            }

        }

        public bool DeleteMessageSchedule(MessageScheduleModel message)
        {
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                using (SqlCommand cmd = new SqlCommand(DeleteMessageScheduleQuery, conn))
                {
                    cmd.Parameters.AddWithValue("@MessageScheduleId", message.MessageScheduleId);
                    bool succeeded = false;
                    try
                    {
                        conn.Open();
                        cmd.Transaction = conn.BeginTransaction();
                        int affectedRows = cmd.ExecuteNonQuery();

                        if (affectedRows != 1)
                        {
                            cmd.Transaction.Rollback();
                        }
                        else
                        {
                            cmd.Transaction.Commit();
                            succeeded = true;
                        }

                    }
                    catch (SqlException ex)
                    {
                        Console.Error.WriteLine(ex.Message);
                    }
                    catch (Exception ex)
                    {
                        Console.Error.WriteLine(ex.Message);
                    }
                    conn.Close();
                    return succeeded;
                }
            }
        }

    }
}