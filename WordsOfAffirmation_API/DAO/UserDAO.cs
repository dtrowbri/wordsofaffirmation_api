﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WordsOfAffirmation_API.Models;
using System.Data.SqlClient;
using System.Configuration;

namespace WordsOfAffirmation_API.DAO
{
    public class UserDAO
    {

        private string connectionString = ConfigurationManager.ConnectionStrings["WOA_Connection_String"].ConnectionString;

        private string UserInsertQuery = "Insert into Users(Username,EmailAddress, PhoneNumber, Password, Enabled) values (@Username, @EmailAddress, @PhoneNumber, @Password, @Enabled)";

        private string UserUpdateQuery = "Update Users set PhoneNumber = @PhoneNumber, Password = @Password, Enabled = @Enabled where UserId = @UserId";

        private string AuthenticateQuery = "Select UserId, Username, EmailAddress, PhoneNumber, Password, Enabled from Users where username = @Username and password = @Password and Enabled = 1";
        
        public bool AddUser(UserModel user)
        {
            using(SqlConnection conn = new SqlConnection(connectionString))
            {
                using (SqlCommand cmd = new SqlCommand(UserInsertQuery, conn))
                {

                    cmd.Parameters.AddWithValue("@Username", user.UserName);
                    cmd.Parameters.AddWithValue("@EmailAddress", user.EmailAddress);
                    cmd.Parameters.AddWithValue("@PhoneNumber", user.PhoneNumber);
                    cmd.Parameters.AddWithValue("@Password", user.Password);
                    cmd.Parameters.AddWithValue("@Enabled", user.Enabled);

                    bool succeeded = false;
                    try
                    {
                        conn.Open();
                        cmd.Transaction = conn.BeginTransaction();
                        int rowsAffected = cmd.ExecuteNonQuery();
                        if(rowsAffected != 1)
                        {
                            cmd.Transaction.Rollback();
                            succeeded = false;
                        } else
                        {
                            cmd.Transaction.Commit();
                            succeeded = true;
                        }

                        conn.Close();
                        return succeeded;
                    }catch(SqlException ex)
                    {
                        Console.Error.WriteLine(ex.Message);
                    }
                    catch(Exception ex)
                    {
                        Console.Error.WriteLine(ex.Message);
                    }
                    conn.Close();
                    return false;
                }
            }
        }

        public bool UpdateUser(UserModel user)
        {
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                using (SqlCommand cmd = new SqlCommand(UserUpdateQuery, conn))
                {

                    cmd.Parameters.AddWithValue("@UserId", user.UserId);
                    cmd.Parameters.AddWithValue("@PhoneNumber", user.PhoneNumber);
                    cmd.Parameters.AddWithValue("@Password", user.Password);
                    cmd.Parameters.AddWithValue("@Enabled", user.Enabled);

                    bool succeeded = false;
                    try
                    {
                        conn.Open();
                        cmd.Transaction = conn.BeginTransaction();
                        int rowsAffected = cmd.ExecuteNonQuery();
                        if (rowsAffected != 1)
                        {
                            cmd.Transaction.Rollback();
                            succeeded = false;
                        }
                        else
                        {
                            cmd.Transaction.Commit();
                            succeeded = true;
                        }

                        conn.Close();
                        return succeeded;
                    }
                    catch (SqlException ex)
                    {
                        Console.Error.WriteLine(ex.Message);
                    }
                    catch (Exception ex)
                    {
                        Console.Error.WriteLine(ex.Message);
                    }
                    conn.Close();
                    return false;
                }
            }
        }

        public UserModel Authenticate(UserModel user)
        {
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                using (SqlCommand cmd = new SqlCommand(AuthenticateQuery, conn))
                {

                    cmd.Parameters.AddWithValue("@Username", user.UserName);
                    cmd.Parameters.AddWithValue("@Password", user.Password);

                    try
                    {
                        conn.Open();
                        SqlDataReader reader = cmd.ExecuteReader();

                        if (reader.Read())
                        {
                            UserModel authenticatedUser = new UserModel();
                            authenticatedUser.UserId = Convert.ToInt32(reader["UserId"]);
                            authenticatedUser.UserName = reader["Username"].ToString();
                            authenticatedUser.PhoneNumber = reader["PhoneNumber"].ToString();
                            authenticatedUser.EmailAddress = reader["EmailAddress"].ToString();
                            authenticatedUser.Enabled = Convert.ToBoolean(reader["Enabled"]);
                            conn.Close();
                            return authenticatedUser;
                        }

                    }
                    catch (SqlException ex)
                    {
                        Console.Error.WriteLine(ex.Message);
                    }
                    catch (Exception ex)
                    {
                        Console.Error.WriteLine(ex.Message);
                    }
                    conn.Close();
                    return null;
                }
            }
        }
    }
}