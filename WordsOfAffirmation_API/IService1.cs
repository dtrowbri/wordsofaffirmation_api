﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using WordsOfAffirmation_API.Models;


namespace WordsOfAffirmation_API
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IService1" in both code and config file together.
    [ServiceContract]
    public interface IService1
    {

        [OperationContract]
        string GetData(int value);

        [OperationContract]
        [WebGet(ResponseFormat = WebMessageFormat.Json, UriTemplate = "version")]
        string GetVersion();

        /*
         *  Users
         */
        [OperationContract]
        [WebInvoke(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "users", Method = "POST")]
        string AddUser(UserModel user);

        [OperationContract]
        [WebInvoke(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "users", Method = "PUT")]
        string UpdateUser(UserModel user);

        [OperationContract]
        [WebInvoke(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "authenticate", Method = "POST")]
        UserModel authenticate(UserModel user);

        /*
         *  Messages
         */
        [OperationContract]
        [WebInvoke(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "messages", Method = "POST")]
        string AddMessage(MessageModel message);

        [OperationContract]
        [WebGet(ResponseFormat = WebMessageFormat.Json, UriTemplate = "messages/{userid}")]
        List<MessageModel> getUserMessages(string userid);

        [OperationContract]
        [WebInvoke(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "messages", Method = "PUT")]
        string UpdateMessage(MessageModel message);

        /*
         *  BasicMessage
         */
        [OperationContract]
        [WebGet(ResponseFormat = WebMessageFormat.Json, UriTemplate = "basicmessages")]
        List<BasicMessageModel> getBasicMessages();

        [OperationContract]
        [WebInvoke(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "basicmessages", Method = "POST")]
        string AddBasicMessage(BasicMessageModel message);

        [OperationContract]
        [WebInvoke(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "basicmessages", Method = "PUT")]
        string UpdateBasicMessage(BasicMessageModel message);

        [OperationContract]
        [WebInvoke(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "basicmessages", Method = "DELETE")]
        string DeleteBasicMessage(BasicMessageModel message);

        /*
         *  MessageLog
         */
        [OperationContract]
        [WebGet(ResponseFormat = WebMessageFormat.Json, UriTemplate = "messagelogs")]
        List<MessageLogsModel> GetMessageLogs();

        [OperationContract]
        [WebInvoke(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "messagelogs", Method = "POST")]
        string AddMessageLog(MessageLogsModel log);

        /*
         *  MessageSchedule
         */
        [OperationContract]
        [WebInvoke(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "scheduledmessages", Method = "POST")]
        string AddMessageSchedule(MessageScheduleModel sched);

        [OperationContract]
        [WebInvoke(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "scheduledmessages", Method = "PUT")]
        string UpdateMessageSchedule(MessageScheduleModel sched);

        [OperationContract]
        [WebInvoke(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "scheduledmessages", Method = "DELETE")]
        string DeleteMessageSchedule(MessageScheduleModel sched);
    }
}
