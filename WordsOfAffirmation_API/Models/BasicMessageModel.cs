﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WordsOfAffirmation_API.Models
{
    public class BasicMessageModel
    {
        public int MessageId { get; set; }
        public string Message { get; set; }

    }
}