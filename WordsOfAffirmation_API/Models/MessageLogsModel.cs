﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WordsOfAffirmation_API.Models
{
    public class MessageLogsModel
    {
        public int LogId { get; set; }
        public int UserId { get; set; }

        public string Message { get; set; }
        public string Recipient { get; set; }
        public DateTime DeliveredOn { get; set; }

    }
}