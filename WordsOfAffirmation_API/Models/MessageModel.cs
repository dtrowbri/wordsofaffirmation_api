﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WordsOfAffirmation_API.Models
{
    public class MessageModel
    {
        public int MessageId { get; set;}
        public int UserId { get; set; }
        public string RecipientName { get; set; }
        public string RecipientPhoneNumber { get; set; }
        public string Message { get; set; }
        public int Frequency { get; set; }
        public bool Archived { get; set; }

    }
}