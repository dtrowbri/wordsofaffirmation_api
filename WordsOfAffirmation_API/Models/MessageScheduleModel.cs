﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WordsOfAffirmation_API.Models
{
    public class MessageScheduleModel
    {
        public int MessageScheduleId { get; set; }
        public int MessageId { get; set; }
        public DateTime NextSendTime { get; set; }
    }
}