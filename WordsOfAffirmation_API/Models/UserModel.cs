﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WordsOfAffirmation_API.Models
{
    public class UserModel
    {
        public int UserId { get; set; }
        public string UserName { get; set; }
        public string EmailAddress { get; set; }
        public string PhoneNumber { get; set; }
        public string Password { get; set; }
        public bool Enabled { get; set; }
    }
}