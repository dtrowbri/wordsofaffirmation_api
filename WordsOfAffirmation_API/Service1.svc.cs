﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using WordsOfAffirmation_API.DAO;
using WordsOfAffirmation_API.Models;

namespace WordsOfAffirmation_API
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Service1" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select Service1.svc or Service1.svc.cs at the Solution Explorer and start debugging.
    public class Service1 : IService1
    {
        public string GetData(int value)
        {
            return string.Format("You entered: {0}", value);
        }

        public string GetVersion()
        {
            return "This is version 1";
        }

        /*
         *  Users
         */
        public string AddUser(UserModel user)
        {
            UserDAO dao = new UserDAO();
            return (dao.AddUser(user)).ToString();
        }

        public string UpdateUser(UserModel user)
        {
            return (new UserDAO()).UpdateUser(user).ToString();
        }

        public UserModel authenticate(UserModel user)
        {
            return (new UserDAO()).Authenticate(user);
        }


        /*
         * Messages
         */
        public string AddMessage(MessageModel message)
        {
            return (new MessageDAO()).AddMessage(message).ToString();
        }

        public List<MessageModel> getUserMessages(string userid)
        {
            
            return (new MessageDAO()).getUserMessages(Convert.ToInt32(userid));
        }

        public string UpdateMessage(MessageModel message)
        {
            return (new MessageDAO()).updateUserMessage(message).ToString();
        }

        /*
         *  Basic Message
         */
        public List<BasicMessageModel> getBasicMessages()
        {
            return (new BasicMessageDAO()).GetBasicMessages();
        }

        public string AddBasicMessage(BasicMessageModel message)
        {
            return (new BasicMessageDAO()).AddBasicMessage(message).ToString();
        }

        public string UpdateBasicMessage(BasicMessageModel message)
        {
            return (new BasicMessageDAO()).UpdateBasicMessage(message).ToString();
        }

        public string DeleteBasicMessage(BasicMessageModel message)
        {
            return (new BasicMessageDAO()).DeleteBasicMessage(message).ToString();
        }

        /*
         * Message Log
         */
        public List<MessageLogsModel> GetMessageLogs()
        {
            return (new MessageLogsDAO()).GetMessageLogs();
        }

        public string AddMessageLog(MessageLogsModel log)
        {
            return (new MessageLogsDAO()).AddMessageLog(log).ToString();
        }
    
        /*
         * Message Schedule
         */

        public string AddMessageSchedule(MessageScheduleModel sched)
        {
            return (new MessageScheduleDAO()).AddMessageSchedule(sched).ToString();
        }

        public string UpdateMessageSchedule(MessageScheduleModel sched)
        {
            return (new MessageScheduleDAO()).UpdateMessageSchedule(sched).ToString();
        }

        public string DeleteMessageSchedule(MessageScheduleModel sched)
        {
            return (new MessageScheduleDAO()).DeleteMessageSchedule(sched).ToString();
        }
    }
}
